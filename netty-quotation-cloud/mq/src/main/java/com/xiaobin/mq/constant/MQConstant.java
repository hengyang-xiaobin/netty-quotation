package com.xiaobin.mq.constant;

/**
 * RocketMQ常量类
 */
public class MQConstant {

	/**
	 * 主题常量
	 */
	public static class Topic {
		// netty消息主题
		public static final String NETTY_TOPIC = "NETTY_MSG_TOPIC";
	}

	/**
	 * 标签常量
	 */
	public static class Tag {
		// 广播消息
		public static final String NETTY_TOPIC_TAG = "NETTY_BROADCAST_MSG_TAG";
	}

	/**
	 * 消费者组常量
	 */
	public static class ConsumerGroup {
		public static final String NETTY_TOPIC_CONSUMER_GROUP = "NETTY_BROADCAST_CONSUMER_GROUP";
	}
}