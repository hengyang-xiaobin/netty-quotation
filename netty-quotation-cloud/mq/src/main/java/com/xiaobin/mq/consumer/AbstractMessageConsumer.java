package com.xiaobin.mq.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.List;

/**
 * 抽象消息消费者 提供基础的消费者配置和初始化功能，具体的消息处理逻辑由子类实现
 */
@Slf4j
public abstract class AbstractMessageConsumer
		implements InitializingBean, DisposableBean {

	/**
	 * 消费者
	 */
	protected DefaultMQPushConsumer consumer;

	/**
	 * 构造函数
	 *
	 * @param consumer
	 *            消费者
	 */
	public AbstractMessageConsumer(DefaultMQPushConsumer consumer) {
		this.consumer = consumer;
	}

	/**
	 * 初始化消费者
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		// 设置消费者组
		consumer.setConsumerGroup(getConsumerGroup());
		// 设置消费模式
		if (getMessageModel() != null) {
			consumer.setMessageModel(getMessageModel());
		}
		// 订阅主题和标签
		consumer.subscribe(getTopic(), getTag());
		// 设置消息监听器
		consumer.registerMessageListener(getMessageListener());
		// 启动消费者
		consumer.start();
		log.info("消费者启动成功，consumerGroup:{}, topic:{}, tag:{}",
				getConsumerGroup(), getTopic(), getTag());
	}

	/**
	 * 获取消息监听器 默认实现为并发消费监听器，子类可以覆盖此方法提供自定义的监听器
	 */
	protected MessageListenerConcurrently getMessageListener() {
		return new MessageListenerConcurrently() {
			@Override
			public ConsumeConcurrentlyStatus consumeMessage(
					List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
				for (MessageExt msg : msgs) {
					try {
						processMessage(msg);
					} catch (Exception e) {
						log.error("消息处理失败，topic:{}, tag:{}, msgId:{}",
								msg.getTopic(), msg.getTags(), msg.getMsgId(),
								e);
						return ConsumeConcurrentlyStatus.RECONSUME_LATER;
					}
				}
				return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
			}
		};
	}

	/**
	 * 销毁消费者
	 */
	@Override
	public void destroy() throws Exception {
		if (consumer != null) {
			consumer.shutdown();
			log.info("消费者关闭成功，consumerGroup:{}, topic:{}, tag:{}",
					getConsumerGroup(), getTopic(), getTag());
		}
	}

	/**
	 * 获取消费者组
	 */
	protected abstract String getConsumerGroup();

	/**
	 * 获取主题
	 */
	protected abstract String getTopic();

	/**
	 * 获取标签
	 */
	protected abstract String getTag();

	protected abstract MessageModel getMessageModel();

	/**
	 * 处理消息
	 */
	protected abstract void processMessage(MessageExt msg);
}