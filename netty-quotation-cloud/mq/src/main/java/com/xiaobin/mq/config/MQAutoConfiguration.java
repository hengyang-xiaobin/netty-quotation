package com.xiaobin.mq.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * MQ模块自动配置类
 */
@Configuration
@ComponentScan("com.xiaobin.mq")
@Import(RocketMQConfig.class)
public class MQAutoConfiguration {
} 