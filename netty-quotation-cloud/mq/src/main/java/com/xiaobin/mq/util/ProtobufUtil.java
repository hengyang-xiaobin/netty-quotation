package com.xiaobin.mq.util;

import com.google.protobuf.Message;
import com.xiaobin.im.protobuf.IMMessage;
import lombok.extern.slf4j.Slf4j;

/**
 * Protobuf工具类
 */
@Slf4j
public class ProtobufUtil {

	/**
	 * 创建IMMessageR消息
	 *
	 * @param type
	 *            消息类型
	 * @param messageTime
	 *            消息时间
	 * @param message
	 *            消息体
	 * @return IMMessageR
	 */
	public static IMMessage.IMMessageR createIMMessageR(int type,
			long messageTime, Message message) {
		IMMessage.MessageHeader header = IMMessage.MessageHeader.newBuilder()
				.setType(type).setMessageTime(messageTime).build();

		IMMessage.IMMessageR.Builder builder = IMMessage.IMMessageR.newBuilder()
				.setMessageHeader(header);

		if (message != null) {
			builder.setMessage(message.toByteString());
		}

		return builder.build();
	}
}