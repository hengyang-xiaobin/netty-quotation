package com.xiaobin.mq.producer;

import com.xiaobin.im.protobuf.IMMessage;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

/**
 * 自定义消息生产者 可以指定使用哪个生产者
 */
@Slf4j
public class CustomMessageProducer implements MessageProducer {

	/**
	 * 生产者
	 */
	private final DefaultMQProducer producer;

	/**
	 * 构造函数
	 *
	 * @param producer
	 *            生产者
	 */
	public CustomMessageProducer(DefaultMQProducer producer) {
		this.producer = producer;
	}

	@Override
	public SendResult sendMessage(String topic, String tag, byte[] body) {
		try {
			Message message = new Message(topic, tag, body);
			SendResult sendResult = producer.send(message);
			log.info("消息发送成功，topic: {}, tag: {}, msgId: {}", topic, tag,
					sendResult.getMsgId());
			return sendResult;
		} catch (Exception e) {
			log.error("消息发送失败，topic: {}, tag: {}", topic, tag, e);
			throw new RuntimeException("消息发送失败", e);
		}
	}

	@Override
	public SendResult sendMessage(String topic, String tag,
			IMMessage.IMMessageR imMessageR) {
		try {
			Message message = new Message(topic, tag, imMessageR.toByteArray());
			SendResult sendResult = producer.send(message);
			log.info("消息发送成功，topic: {}, tag: {}, msgId: {}", topic, tag,
					sendResult.getMsgId());
			return sendResult;
		} catch (Exception e) {
			log.error("消息发送失败，topic: {}, tag: {}", topic, tag, e);
			throw new RuntimeException("消息发送失败", e);
		}
	}

	@Override
	public SendResult sendMessage(String topic, String tag, String key,
			byte[] body) {
		try {
			Message message = new Message(topic, tag, key, body);
			SendResult sendResult = producer.send(message);
			log.info("消息发送成功，topic: {}, tag: {}, key: {}, msgId: {}", topic,
					tag, key, sendResult.getMsgId());
			return sendResult;
		} catch (Exception e) {
			log.error("消息发送失败，topic: {}, tag: {}, key: {}", topic, tag, key, e);
			throw new RuntimeException("消息发送失败", e);
		}
	}

	@Override
	public SendResult sendDelayMessage(String topic, String tag, String key,
			byte[] body, int delayLevel) {
		try {
			Message message = new Message(topic, tag, key, body);
			// 设置延时级别
			message.setDelayTimeLevel(delayLevel);
			SendResult sendResult = producer.send(message);
			log.info(
					"延时消息发送成功，topic: {}, tag: {}, key: {}, delayLevel: {}, msgId: {}",
					topic, tag, key, delayLevel, sendResult.getMsgId());
			return sendResult;
		} catch (Exception e) {
			log.error("延时消息发送失败，topic: {}, tag: {}, key: {}, delayLevel: {}",
					topic, tag, key, delayLevel, e);
			throw new RuntimeException("延时消息发送失败", e);
		}
	}
}