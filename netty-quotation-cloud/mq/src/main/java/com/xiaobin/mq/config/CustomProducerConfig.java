package com.xiaobin.mq.config;

import com.xiaobin.mq.producer.CustomMessageProducer;
import com.xiaobin.mq.producer.MessageProducer;
import lombok.Data;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author ningbin
 * @date 2025/3/14
 */
@Data
@Configuration
@ConditionalOnClass(RocketMQConfig.class)
public class CustomProducerConfig {

	@Bean
	public MessageProducer defaultProducer(
			@Autowired DefaultMQProducer defaultMQProducer) {
		return new CustomMessageProducer(defaultMQProducer);
	}

}
