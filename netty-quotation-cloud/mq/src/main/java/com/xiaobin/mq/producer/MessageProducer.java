package com.xiaobin.mq.producer;

import com.xiaobin.im.protobuf.IMMessage;
import org.apache.rocketmq.client.producer.SendResult;

/**
 * 消息生产者接口
 */
public interface MessageProducer {

	/**
	 * 发送消息
	 *
	 * @param topic
	 *            主题
	 * @param tag
	 *            标签
	 * @param body
	 *            消息体
	 * @return 发送结果
	 */
	SendResult sendMessage(String topic, String tag, byte[] body);

	/**
	 * 发送消息
	 *
	 * @param topic
	 *            主题
	 * @param tag
	 *            标签
	 * @param imMessageR
	 *            im消息
	 * @return 发送结果
	 */
	SendResult sendMessage(String topic, String tag,
			IMMessage.IMMessageR imMessageR);

	/**
	 * 发送消息（带消息键）
	 *
	 * @param topic
	 *            主题
	 * @param tag
	 *            标签
	 * @param key
	 *            消息键
	 * @param body
	 *            消息体
	 * @return 发送结果
	 */
	SendResult sendMessage(String topic, String tag, String key, byte[] body);

	/**
	 * 发送延迟消息
	 *
	 * @param topic
	 *            主题
	 * @param tag
	 *            标签
	 * @param key
	 *            消息键
	 * @param body
	 *            消息体
	 * @param delayLevel
	 *            延时级别
	 * @return 发送结果
	 */
	SendResult sendDelayMessage(String topic, String tag, String key,
			byte[] body, int delayLevel);
}
