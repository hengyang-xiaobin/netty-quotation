package com.xiaobin.mq.config;

import lombok.Data;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.annotation.Value;

/**
 * RocketMQ配置类
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "rocketmq")
public class RocketMQConfig {

	@ConditionalOnProperty(prefix = "rocketmq", value = "producerGroup")
	@Bean(destroyMethod = "shutdown")
	public DefaultMQProducer defaultMQProducer(
			@Value("${rocketmq.nameServerAddr}") String nameServerAddr,
			@Value("${rocketmq.producerGroup}") String producerGroup,
			@Value("${rocketmq.maxMessageSize:4194304}") Integer maxMessageSize,
			@Value("${rocketmq.sendMsgTimeout:3000}") Integer sendMsgTimeout,
			@Value("${rocketmq.retryTimesWhenSendFailed:2}") Integer retryTimesWhenSendFailed)
			throws MQClientException {
		DefaultMQProducer producer = createProducer(nameServerAddr,
				producerGroup, maxMessageSize, sendMsgTimeout,
				retryTimesWhenSendFailed);
		producer.start();
		return producer;
	}

	@ConditionalOnProperty(prefix = "rocketmq", value = "consumerGroup")
	@Bean(destroyMethod = "shutdown")
	public DefaultMQPushConsumer defaultMQPushConsumer(
			@Value("${rocketmq.nameServerAddr}") String nameServerAddr,
			@Value("${rocketmq.consumerGroup}") String consumerGroup) {
		DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(
				consumerGroup);
		consumer.setNamesrvAddr(nameServerAddr);
		consumer.setVipChannelEnabled(false);
		// 消费者初始化后需要订阅主题，在具体的消费者实现类中进行
		return consumer;
	}

	private DefaultMQProducer createProducer(String nameServerAddr,
			String producerGroup, Integer maxMessageSize,
			Integer sendMsgTimeout, Integer retryTimesWhenSendFailed) {
		DefaultMQProducer producer = new DefaultMQProducer(producerGroup);
		producer.setNamesrvAddr(nameServerAddr);
		producer.setMaxMessageSize(maxMessageSize);
		producer.setSendMsgTimeout(sendMsgTimeout);
		producer.setRetryTimesWhenSendFailed(retryTimesWhenSendFailed);
		producer.setVipChannelEnabled(false);
		return producer;
	}

}