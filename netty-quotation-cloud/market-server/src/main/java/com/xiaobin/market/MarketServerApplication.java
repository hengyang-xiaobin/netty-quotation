package com.xiaobin.market;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * Market Server应用程序入口
 */
@SpringBootApplication
@ComponentScan({"com.xiaobin.market", "com.xiaobin.mq"})
public class MarketServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(MarketServerApplication.class, args);
    }
} 