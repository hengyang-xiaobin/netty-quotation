package com.xiaobin.netty;

import com.xiaobin.im.protobuf.IMMessage;
import com.xiaobin.mq.constant.MQConstant;
import com.xiaobin.mq.producer.MessageProducer;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;

@Slf4j
public class WebsocketServerHandler
		extends SimpleChannelInboundHandler<IMMessage.IMMessageR> {

	private MessageProducer messageProducer;

	public WebsocketServerHandler(MessageProducer messageProducer) {
		this.messageProducer = messageProducer;
	}

	public static String getChannelKey(Channel channel) {
		InetSocketAddress socketAddress = (InetSocketAddress) channel
				.remoteAddress();
		String longId = channel.id().asLongText();
		String hasCode = channel.hashCode() + "";
		String host = socketAddress.getHostString();
		int port = socketAddress.getPort();
		return String.format("%s:%s:%s:%s", longId, host, port, hasCode);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx,
			IMMessage.IMMessageR msg) throws Exception {
		IMMessage.MessageHeader messageHeader = msg.getMessageHeader();

		// 将消息发送到MQ模块业务处理，MQ集群部署，消息需要单一发送
		if (messageProducer != null) {
			try {
				// 发送消息到MQ
				messageProducer.sendMessage(MQConstant.Topic.NETTY_TOPIC,
						MQConstant.Tag.NETTY_TOPIC_TAG, msg);
			} catch (Exception e) {
				log.error("发送消息到MQ失败", e);
			}
		} else {
			log.warn("MessageService未注入，无法发送消息到MQ");
		}
	}

	/**
	 * 连接断开的时候触发的操作
	 *
	 * @param ctx
	 *            通道上下文
	 * @throws Exception
	 */
	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
		log.info("连接断开的时候触发的操作", ctx.channel().toString());
		String channelKey = getChannelKey(ctx.channel());
	}

	/**
	 * channel 断开连接的channel
	 *
	 * @param ctx
	 *            通道上下文
	 * @throws Exception
	 */
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		log.info("断开连接的channel", ctx.channel().toString());
	}

	/**
	 * 异常信息捕获
	 *
	 * @param ctx
	 *            通道上下文
	 * @param cause
	 *            异常信息
	 * @throws Exception
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		log.error("WebSocketNettyServerHandler-exceptionCaught-Exception",
				cause);
		String channelKey = getChannelKey(ctx.channel());
		ctx.channel().close();
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt)
			throws Exception {
		if (evt instanceof IdleStateEvent) {
			IdleStateEvent event = (IdleStateEvent) evt;
			switch (event.state()) {
			case READER_IDLE:
				log.info(
						"WebSocketNettyServerHandler-userEventTriggered-没有接收到:{}-的信息心跳信息，将断开连接回收资源",
						ctx.toString());
				break;
			case WRITER_IDLE:
				log.info("WebSocketNettyServerHandler-userEventTriggered-写空闲");
				break;
			case ALL_IDLE:
				log.info("WebSocketNettyServerHandler-userEventTriggered-读写空闲");
				break;
			default:
				log.info("WebSocketNettyServerHandler-userEventTriggered-非法状态");
				throw new IllegalStateException("非法状态！");
			}
		}
	}

	@Override
	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
		String channelKey = getChannelKey(ctx.channel());
	}
}
