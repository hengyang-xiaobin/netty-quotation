package com.xiaobin.netty;

import java.util.concurrent.TimeUnit;

import com.xiaobin.im.protobuf.IMMessage;
import com.xiaobin.mq.producer.MessageProducer;
import com.xiaobin.netty.encode.MessageProtobufDecoder;
import com.xiaobin.netty.encode.MessageProtobufEncoder;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;

public class WebsocketServerInit extends ChannelInitializer<NioSocketChannel> {

	private String url;

	private MessageProducer messageProducer;

	public WebsocketServerInit(String url, MessageProducer messageProducer) {
		this.url = url;
		this.messageProducer = messageProducer;
	}

	@Override
	protected void initChannel(NioSocketChannel channel) throws Exception {
		ChannelPipeline pipeline = channel.pipeline();

		// HTTP请求的解码和编码
		pipeline.addLast("httpServerCodec", new HttpServerCodec());
		// 把多个消息转换为一个单一的FullHttpRequest或是FullHttpResponse，
		// 原因是HTTP解码器会在每个HTTP消息中生成多个消息对象HttpRequest/HttpResponse,HttpContent,LastHttpContent
		pipeline.addLast("httpObjectAggregator",
				new HttpObjectAggregator(65536));
		// 主要用于处理大数据流，比如一个1G大小的文件如果你直接传输肯定会撑暴jvm内存的; 增加之后就不用考虑这个问题了
		pipeline.addLast("chunkedWriteHandler", new ChunkedWriteHandler());
		// 心跳机制
		pipeline.addLast(new IdleStateHandler(10, 60, 0, TimeUnit.SECONDS));
		// WebSocket数据压缩
		pipeline.addLast("webSocketServerCompressionHandler",
				new WebSocketServerCompressionHandler());
		pipeline.addLast("webSocketServerProtocolHandler",
				new WebSocketServerProtocolHandler(this.url, null, true,
						1024 * 10));
		// 编码解码
		// 解码器，通过Google Protocol Buffers序列化框架动态的切割接收到的ByteBuf
		pipeline.addLast("messageProtobufDecoder",
				new MessageProtobufDecoder());
		pipeline.addLast("messageProtobufEncoder",
				new MessageProtobufEncoder());

		pipeline.addLast("protobufDecoder",
				new ProtobufDecoder(IMMessage.IMMessageR.getDefaultInstance()));

		pipeline.addLast(new ProtobufEncoder());

		// 添加自定义处理器
		pipeline.addLast("websocketServerHandler",
				new WebsocketServerHandler(messageProducer));

	}

}
