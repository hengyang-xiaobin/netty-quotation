package com.xiaobin.netty;

import com.xiaobin.mq.producer.MessageProducer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ningbin
 * @date 2022/6/23
 */
@Slf4j
@Component
public class NettyServer {

	@Autowired
	private MessageProducer messageProducer;

	private NioEventLoopGroup parentGroup;
	private NioEventLoopGroup childGroup;

	public ChannelFuture start() throws Exception {
		int nettyPort = 8898;
		log.info("netty server port:{}", nettyPort);
		parentGroup = new NioEventLoopGroup(1);
		childGroup = new NioEventLoopGroup();

		ServerBootstrap serverBootstrap = new ServerBootstrap()
				.group(parentGroup, childGroup)
				.handler(new LoggingHandler(LogLevel.INFO))
				.channel(NioServerSocketChannel.class)
				.childHandler(new WebsocketServerInit("/ws", messageProducer));
		ChannelFuture channelFuture = serverBootstrap.bind(nettyPort).sync();
		return channelFuture;
	}

	public void shutdown() {
		log.info("netty server close");
		childGroup.shutdownGracefully();
		parentGroup.shutdownGracefully();
	}

}
