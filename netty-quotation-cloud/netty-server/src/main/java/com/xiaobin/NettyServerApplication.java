package com.xiaobin;

import com.xiaobin.netty.NettyServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * Netty Server应用程序入口
 */
@SpringBootApplication
@ComponentScan({"com.xiaobin", "com.xiaobin.mq"})
public class NettyServerApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(NettyServerApplication.class, args);
        
        try {
            // 启动Netty服务器
            NettyServer nettyServer = context.getBean(NettyServer.class);
            nettyServer.start();
            
            // 添加关闭钩子
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                nettyServer.shutdown();
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
