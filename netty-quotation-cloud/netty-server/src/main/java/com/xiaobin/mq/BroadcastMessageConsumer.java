package com.xiaobin.mq;

import com.xiaobin.mq.constant.MQConstant;
import com.xiaobin.mq.consumer.AbstractMessageConsumer;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ningbin
 * @date 2025/3/14
 */

@Component
public class BroadcastMessageConsumer extends AbstractMessageConsumer {

	public BroadcastMessageConsumer(@Autowired DefaultMQPushConsumer consumer) {
		super(consumer);
	}

	@Override
	protected String getConsumerGroup() {
		return MQConstant.ConsumerGroup.NETTY_TOPIC_CONSUMER_GROUP;
	}

	@Override
	protected String getTopic() {
		return MQConstant.Topic.NETTY_TOPIC;
	}

	@Override
	protected String getTag() {
		return MQConstant.Tag.NETTY_TOPIC_TAG;
	}

	@Override
	protected MessageModel getMessageModel() {
		return MessageModel.CLUSTERING;
	}

	@Override
	protected void processMessage(MessageExt msg) {
		System.out.println("处理消息");
	}
}
