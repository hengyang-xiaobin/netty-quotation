package com.xiaobin.netty;

import com.xiaobin.im.protobuf.IMMessage;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

/**
 * Netty客户端测试类
 */
public class NettyClientTest {

    private final String HOST = "localhost";
    private final int PORT = 8899;
    private final String WEBSOCKET_PATH = "/ws";

    /**
     * 主方法，用于启动客户端测试
     */
    public static void main(String[] args) throws Exception {
        NettyClientTest client = new NettyClientTest();
        client.testWebSocketClient();
    }

    /**
     * 测试WebSocket客户端
     */
    public void testWebSocketClient() throws Exception {
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            // 创建WebSocket URI
            URI uri = new URI("ws://" + HOST + ":" + PORT + WEBSOCKET_PATH);
            
            // 创建WebSocket握手处理器
            WebSocketClientHandshaker handshaker = WebSocketClientHandshakerFactory.newHandshaker(
                    uri, WebSocketVersion.V13, null, true, new DefaultHttpHeaders());
            
            // 创建客户端处理器
            WebSocketClientHandler handler = new WebSocketClientHandler(handshaker);
            
            // 配置客户端
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) {
                            ChannelPipeline pipeline = ch.pipeline();
                            // HTTP请求的解码和编码
                            pipeline.addLast("httpClientCodec", new HttpClientCodec());
                            // 把多个消息转换为一个单一的FullHttpRequest或是FullHttpResponse
                            pipeline.addLast("httpObjectAggregator", new HttpObjectAggregator(65536));
                            // 添加WebSocket客户端处理器
                            pipeline.addLast("webSocketClientHandler", handler);
                        }
                    });
            
            // 连接服务器
            Channel channel = bootstrap.connect(HOST, PORT).sync().channel();
            
            // 等待握手完成
            handler.handshakeFuture().sync();
            System.out.println("WebSocket握手成功，连接已建立!");
            
            // 发送消息
            sendMessage(channel);
            
            // 等待连接关闭
            channel.closeFuture().sync();
        } finally {
            // 优雅关闭
            group.shutdownGracefully();
        }
    }
    
    /**
     * 发送测试消息
     */
    private void sendMessage(Channel channel) throws InterruptedException {
        // 创建消息头
        IMMessage.MessageHeader messageHeader = IMMessage.MessageHeader.newBuilder()
                .setType(1) // 消息类型
                .setMessageTime(System.currentTimeMillis()) // 消息时间戳
                .build();
        
        // 创建消息体
        IMMessage.IMMessageR message = IMMessage.IMMessageR.newBuilder()
                .setMessageHeader(messageHeader)
                .setMessage(com.google.protobuf.ByteString.copyFromUtf8("Hello from Netty Client!"))
                .build();
        
        // 将Protobuf消息转换为字节数组
        byte[] bytes = message.toByteArray();
        ByteBuf byteBuf = Unpooled.wrappedBuffer(bytes);
        
        // 使用BinaryWebSocketFrame包装消息
        BinaryWebSocketFrame frame = new BinaryWebSocketFrame(byteBuf);
        
        // 发送消息
        channel.writeAndFlush(frame);
        System.out.println("已发送消息: " + message);
        
        // 等待一段时间接收响应
        TimeUnit.SECONDS.sleep(5);
    }
    
    /**
     * WebSocket客户端处理器
     */
    private static class WebSocketClientHandler extends SimpleChannelInboundHandler<Object> {
        
        private final WebSocketClientHandshaker handshaker;
        private ChannelPromise handshakeFuture;
        
        public WebSocketClientHandler(WebSocketClientHandshaker handshaker) {
            this.handshaker = handshaker;
        }
        
        public ChannelFuture handshakeFuture() {
            return handshakeFuture;
        }
        
        @Override
        public void handlerAdded(ChannelHandlerContext ctx) {
            handshakeFuture = ctx.newPromise();
        }
        
        @Override
        public void channelActive(ChannelHandlerContext ctx) {
            handshaker.handshake(ctx.channel());
        }
        
        @Override
        public void channelInactive(ChannelHandlerContext ctx) {
            System.out.println("WebSocket客户端连接断开!");
        }
        
        @Override
        protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
            Channel ch = ctx.channel();
            
            // 处理WebSocket握手
            if (!handshaker.isHandshakeComplete()) {
                handshaker.finishHandshake(ch, (FullHttpResponse) msg);
                System.out.println("WebSocket客户端握手完成!");
                handshakeFuture.setSuccess();
                return;
            }
            
            // 处理WebSocket帧
            if (msg instanceof WebSocketFrame) {
                handleWebSocketFrame(ctx, (WebSocketFrame) msg);
            }
        }
        
        private void handleWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame frame) {
            // 处理关闭帧
            if (frame instanceof CloseWebSocketFrame) {
                handshaker.close(ctx.channel(), (CloseWebSocketFrame) frame.retain());
                return;
            }
            
            // 处理Ping帧
            if (frame instanceof PingWebSocketFrame) {
                ctx.channel().writeAndFlush(new PongWebSocketFrame(frame.content().retain()));
                return;
            }
            
            // 处理二进制帧
            if (frame instanceof BinaryWebSocketFrame) {
                BinaryWebSocketFrame binaryFrame = (BinaryWebSocketFrame) frame;
                ByteBuf content = binaryFrame.content();
                
                try {
                    // 尝试解析为IMMessage.IMMessageR
                    byte[] bytes = new byte[content.readableBytes()];
                    content.readBytes(bytes);
                    IMMessage.IMMessageR message = IMMessage.IMMessageR.parseFrom(bytes);
                    System.out.println("收到二进制消息，解析为IMMessage: " + message);
                } catch (Exception e) {
                    System.out.println("解析二进制消息失败: " + e.getMessage());
                    System.out.println("原始内容: " + content.toString());
                }
            }
            
            // 处理文本帧
            if (frame instanceof TextWebSocketFrame) {
                TextWebSocketFrame textFrame = (TextWebSocketFrame) frame;
                System.out.println("收到文本消息: " + textFrame.text());
            }
        }
        
        @Override
        public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
            cause.printStackTrace();
            if (!handshakeFuture.isDone()) {
                handshakeFuture.setFailure(cause);
            }
            ctx.close();
        }
    }
} 