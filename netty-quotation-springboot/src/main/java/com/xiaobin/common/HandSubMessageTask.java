package com.xiaobin.common;

/**
 * @author xiaobin 
 * @date 2020/4/4 20:34
 * @desc 处理新的订阅消息
 */
public interface HandSubMessageTask{
    public void process();
}
