package com.xiaobin.exception;

/**
 * @author xiaobin 
 * @date 2020/7/12 12:57
 * @desc
 */
public class BaseException extends RuntimeException {

    public BaseException(String message){
        super(message);
    }

}
