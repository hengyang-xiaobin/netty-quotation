package com.xiaobin.exception;

/**
 * @author xiaobin 
 * @date 2020/7/12 12:58
 * @desc
 */
public class HuobiHttpRequestException extends BaseException {

    public HuobiHttpRequestException(String message) {
        super(message);
    }
}
