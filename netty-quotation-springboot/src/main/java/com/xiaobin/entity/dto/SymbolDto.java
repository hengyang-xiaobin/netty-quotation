package com.xiaobin.entity.dto;

import lombok.Data;

/**
 * @author xiaobin 
 * @date 2020/7/15 16:34
 * @desc
 */
@Data
public class SymbolDto {

    private String symbol;
    private String symbolUp;

}
