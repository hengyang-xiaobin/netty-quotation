package com.xiaobin;

import com.xiaobin.handler.context.ApplicationContextProvider;
import com.xiaobin.handler.event.HuoBiMessageEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author xiaobin 
 * @date 2020/4/2 21:28
 * @desc springboot启动类
 */
@SpringBootApplication
@EnableScheduling
public class QuotationApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuotationApplication.class);
    }

}
