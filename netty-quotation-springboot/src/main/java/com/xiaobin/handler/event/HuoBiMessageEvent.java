package com.xiaobin.handler.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author ningbin
 * @date 2024/3/22
 */
public class HuoBiMessageEvent extends ApplicationEvent {
    public HuoBiMessageEvent(String message) {
        super(message);
    }
}
